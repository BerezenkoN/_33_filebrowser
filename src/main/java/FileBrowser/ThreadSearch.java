package FileBrowser;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by user on 15.02.2017.
 */
public class ThreadSearch extends Thread {
    private final String host;
    private final int port;

    public ThreadSearch(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public void run() {
        try {
            Random rnd = new Random();
            DatagramSocket datagramSocket = null;
            while (true) {
                try {
                    datagramSocket = new DatagramSocket(rnd.nextInt(10) + 1024);
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            byte[] buf = new byte[6];
            byte[] searchLable = {10, 5, 21, 48, 52, 33};
            DatagramPacket requestPacket = new DatagramPacket(buf, buf.length);
            while (true) {
                datagramSocket.receive(requestPacket);
                if (requestPacket.getLength() < searchLable.length) continue;
                if (Arrays.equals(searchLable, requestPacket.getData())) {
                    byte[] host_Port = (host + ":" + port).getBytes();
                    DatagramPacket tcpPacket = new DatagramPacket(host_Port, host_Port.length, requestPacket.getAddress(), requestPacket.getPort());
                    datagramSocket.send(tcpPacket);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
package FileBrowser;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.Random;

/**
 * Created by user on 15.02.2017.
 */
public class Client {
    public static final String INTERFACE_IP = "192.168.0.103";
    public static final String INTERFACE_BROADCAST_IP = "192.168.3.255";

    public static void main(String[] args) {
        Random rnd = new Random();
        ServerSocket serverSocket = null;
        while (true) {
            try {
                serverSocket = new ServerSocket(rnd.nextInt(65535), 0, InetAddress.getByName(INTERFACE_IP));
                System.out.println("TCP socket to " + INTERFACE_IP + ":" + serverSocket.getLocalPort());
                ThreadBrowserServer browserServer = new ThreadBrowserServer(serverSocket);
                browserServer.setDaemon(true);
                browserServer.start();
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        ThreadSearch threadSearch = new ThreadSearch(INTERFACE_IP, serverSocket.getLocalPort());
        threadSearch.start();

        ThreadConsol threadConsol = new ThreadConsol();
        threadConsol.start();
    }
}


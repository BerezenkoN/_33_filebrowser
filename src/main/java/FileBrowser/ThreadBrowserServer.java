package FileBrowser;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by user on 15.02.2017.
 */
public class ThreadBrowserServer extends Thread {
    private final ServerSocket serverSocket;

    public ThreadBrowserServer(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }


    @Override
    public void run() {
        try {
            Socket socket = serverSocket.accept();
            ThreadBrowserClient browserClient = new ThreadBrowserClient(socket);
            browserClient.setDaemon(true);
            browserClient.start();
        }catch (IOException e){
            e.printStackTrace();

        }
    }
}

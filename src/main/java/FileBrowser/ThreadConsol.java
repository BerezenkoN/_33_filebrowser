package FileBrowser;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by user on 15.02.2017.
 */
public class ThreadConsol extends Thread {
    private Socket socket;
    private DataInputStream serverInputStream;
    private DataOutputStream serverOutputStream;

    private void broadcast(){
        try {
            DatagramSocket datagramSocket = new DatagramSocket(8887);
            byte[] searchLable = {10, 5, 21, 48, 52, 33};
            DatagramPacket datagramPacket = null;
            for (int port = 1024; port < 1034; port++) {
                datagramPacket = new DatagramPacket(searchLable, searchLable.length, InetAddress.getByName(Client.INTERFACE_BROADCAST_IP), port);
                datagramSocket.send(datagramPacket);
            }
            Thread thread = new Thread(() -> {
                try{
                    byte[] buffer = new byte[255];
                    DatagramPacket ipPortPacket = new DatagramPacket(buffer, buffer.length);
                    while(true){
                        datagramSocket.receive(ipPortPacket);
                        String ipPortStr = new String(ipPortPacket.getData(), 0, ipPortPacket.getLength());
                        String[] ipPort = ipPortStr.split(":");
                        System.out.println(ipPort[0] + ":" + ipPort[1]);
                    }
                }catch (IOException e){}
            });
            System.out.println("Search clients");
            thread.start();

            Thread.sleep(1000);
            System.out.println("Search done");
            thread.interrupt();
            datagramSocket.close();

        }catch (Exception e){
            e.printStackTrace();
        }

    }
    private void connect(String consolCommand){
        String[] ipPort = consolCommand.substring(consolCommand.lastIndexOf(" ") + 1).split(":");
        String portIp = ipPort[0];
        int port = Integer.parseInt(ipPort[1]);

        try{
            socket = new Socket(portIp, port);
            serverInputStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            serverOutputStream = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try{
            BufferedReader reader = new BufferedReader (new InputStreamReader(System.in));
            String consolCommand;
            while (!(consolCommand = reader.readLine()).equals("exit")) {
                if (consolCommand.equals("broadcast")) {
                    broadcast();
                }
                if (consolCommand.startsWith("connect")) {
                    connect(consolCommand);
                    System.out.println("Connected");
                }
                if (consolCommand.equals("ls") && socket != null) {
                    serverOutputStream.writeUTF(consolCommand);
                    serverOutputStream.flush();
                    String files = serverInputStream.readUTF();
                    System.out.println(files);
                }
                if (consolCommand.startsWith("cd") && socket != null) {
                    serverOutputStream.writeUTF(consolCommand);
                    serverOutputStream.flush();
                    String files = serverInputStream.readUTF();
                    System.out.println(files);
                }
                if (consolCommand.equals("pwd") && socket != null) {
                    serverOutputStream.writeUTF(consolCommand);
                    serverOutputStream.flush();
                    String flowingDir = serverInputStream.readUTF();
                    System.out.println(flowingDir);
                }
                if (consolCommand.equals("disconnect") && socket != null) {
                    socket.close();
                    socket = null;
                }

            }


        }catch (Exception e){
            e.printStackTrace();
        }
    }

}

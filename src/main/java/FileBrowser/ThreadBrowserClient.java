package FileBrowser;

import java.io.*;
import java.net.Socket;

/**
 * Created by user on 15.02.2017.
 */
public class ThreadBrowserClient extends Thread {
     private static final String ROOT_DIR = "C:\\";
    private final Socket socket;
    private String cPath = ROOT_DIR;

    private String ls(){
        File cDirectory = new File(cPath);
        StringBuilder sb = new StringBuilder();
        sb.append("..").append("\n");
        File[] f_iles = cDirectory.listFiles();
        if (f_iles != null){
            for(File k : f_iles){
                if (k.isDirectory()) sb.append("(d)");
                sb.append(k.getName()).append("\n");
            }

        }
        return sb.toString();

    }

    private String cd(String dir){
        File targetDirectory = new File(cPath + File.separator + dir);
        if (targetDirectory.isDirectory()){
            if (dir.equals("..") && !cPath.equals(ROOT_DIR))
                cPath = cPath.substring(0, cPath.lastIndexOf(File.separator));
            if (cPath.isEmpty())cPath = ROOT_DIR;
            else{
                if(!cPath.equals(ROOT_DIR)) cPath += File.separator;
                cPath +=dir;

            }
        }
        return ls();

    }

    public ThreadBrowserClient(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run(){
        try{
            System.out.println("Connected Client: " + socket.getRemoteSocketAddress().toString() + ":" + socket.getPort());
            DataInputStream clientIn = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            DataOutputStream clientOut = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));

            while(true){
                String consolCommand = clientIn.readUTF();
                if(consolCommand.equals("ls")){
                    String output = ls();
                    clientOut.writeUTF(output);
                    clientOut.flush();
                }
                if (consolCommand.startsWith("cd")){
                    String output = cd(consolCommand.substring(consolCommand.lastIndexOf(" ") + 1));
                    clientOut.writeUTF(output);
                    clientOut.flush();
                }
                if (consolCommand.equals("pwd")){
                    String output = cPath;
                    clientOut.writeUTF(output);
                    clientOut.flush();
                }
            }

        }catch (IOException e){
            System.out.println("Disconnected client");

            e.printStackTrace();
        }
    }

}
